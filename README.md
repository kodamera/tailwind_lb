# Tailwind Layout Builder
Build content while receiving instant visual feedback. No more endless filling of fields.

Tailwind Layout Builder lets you create content while getting instant visual feedback on what your content will look like.

## Dependencies
This module requires the following modules to work as expected:

- `layout_builder` (core)
- `media` (core)
- `media_library` (core)
- `responsive_image` (core)
- `allowed_formats`
- `block_list_override`
- `inline_block_title_automatic`
- `layout_builder_modal`
- `layout_builder_restrictions`
- `layout_builder_restrictions_by_region`

Installing the module via Composer will take care of these dependencies automatically.

## Getting started

1. Add the following to your project's composer.json under “repositories”:
   ```
   {
      "type": "vcs",
      "url": "https://bitbucket.org/kodamera/tailwind_lb.git"
   }
   ```
2. Require the module and it's dependencies: `./dev.sh composer require kodamera/tailwind_lb`
3. Enable the module: `./dev.sh drush en tailwind_lb`
4. Create a page of type "Sida/Page" -> Navigate to 'layout' and start to create content.

## Generate image styles for use in the module.

In the root folder of your project, run: `./dev.sh drush tailwind_lb:image_styles:generate`.

The image styles are automatically applied to your images on your layout builder pages.

## Available layouts/sections

* One column layout
* Two column layout
* Three column layout

## Available blocks/components

* Image with text (horizontal)
  * Image
  * Image alignment (left/right)
  * Content alignment (top/center/bottom)
  * Label
  * Heading
  * Heading level (h1, h2, h3, h4)
  * Text (wysiwyg minimal)
  * Link
* Image with text (vertical)
  * Image
  * Label
  * Heading
  * Heading level (h1, h2, h3, h4)
  * Text (wysiwyg minimal)
  * Link
* Image with text overlay
  * Image
  * Label
  * Heading
  * Heading level (h1, h2, h3, h4)
  * Text (wysiwyg stripped)
  * Link
* Image
  * Image
* Video
  * Video
  * Remote video
* Text
  * Text (wysiwyg rich)
* Text with applied styles
  * Text (text clean)
  * Label
  * Heading
  * Heading level
  * Link

### Available variables for blocks
* `configuration.layout_id` - The ID of the layout the block is in
* `configuration.is_fullwith` - Is `TRUE` if the layout the block is in is fullwidth
* `is_editing_layout` - Is `TRUE` if the block is shown in the layout builder editing mode

## Integrate with your current theme

* Tailwind Layout Builder's CSS is built with Tailwind CSS which your theme needs to support.
* Make sure to include Tailwind Layout Builder's templates when purging your CSS:

```
purge: {
  mode: 'all',
  content: [
    'templates/**/*.html.twig',
    'src/js/**/*.js',
    '../../../modules/contrib/tailwind_lb/*/**/*.html.twig'
  ],
  options: {
    safelist: [
      'bg-gray-100',
      'md:col-span-3',
      'md:col-span-4',
      'md:col-span-6',
      'md:col-span-8',
      'md:col-span-9',
    ],
  }
},
```

### Safelisted classes
Tailwind Layout Builder contains settings possibilities for sections and components.

The classes listed below 'safelist' above are being generated dynamically and needs to be safelisted to end up in the production stylesheet since Purge CSS can't retrieve the classes in the templates.
