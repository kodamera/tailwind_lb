<?php

namespace Drupal\tailwind_lb\Plugin\Layout;

use Drupal\layout_builder\Plugin\Layout\MultiWidthLayoutBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configurable one column fullwidth layout plugin class.
 *
 * @internal
 *   Plugin classes are internal.
 */
class FullColumnLayout extends MultiWidthLayoutBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration();
  }


  /**
   * {@inheritdoc}
   */
  protected function getWidthOptions() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();

    $form['margin'] = [
      '#type' => 'select',
      '#title' => $this->t('Adjust margin'),
      '#default_value' => $configuration['margin'] ?? 'none',
      '#description' => t('Adds margin to the section to control the spacing to section before/after.'),
      '#options' => [
        'none' => $this->t('None'),
        'top' => $this->t('Add margin top'),
        'bottom' => $this->t('Add margin bottom'),
        'top_bottom' => $this->t('Add margin top and bottom'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['margin'] = $form_state->getValue('margin');
  }
}
