<?php

namespace Drupal\tailwind_lb\Plugin\Layout;

use Drupal\layout_builder\Plugin\Layout\MultiWidthLayoutBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configurable one column layout plugin class.
 *
 * @internal
 *   Plugin classes are internal.
 */
class OneColumnLayout extends MultiWidthLayoutBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'section_heading' => '',
      'heading_level' => 'h2',
      'section_preamble' => '',
      'alignment' => 'text-left',
      'background' => 'transparent',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getWidthOptions() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();
    $form['section_heading'] = [
      '#title' => t('Section heading'),
      '#type' => 'textfield',
      '#description' => t("Section heading is printed above the section's content."),
      '#default_value' => $configuration['section_heading'] ?? '',
      '#size' => 50,
    ];

    $form['heading_level'] = [
      '#type' => 'select',
      '#title' => $this ->t('Section heading level'),
      '#default_value' => $configuration['heading_level'] ?? 'h2',
      '#description' => t('Set section heading level for correct page structure.'),
      '#options' => [
        'h1' => 'H1',
        'h2' => 'H2',
        'h3' => 'H3',
      ],
    ];

    $form['section_preamble'] = [
      '#title' => t('Section preamble'),
      '#type' => 'textarea',
      '#description' => t("Section preamble is printed above the section's content but below the section heading."),
      '#default_value' => $configuration['section_preamble'] ?? '',
    ];

    $form['alignment'] = [
      '#type' => 'select',
      '#title' => $this ->t('Align section heading and section preamble'),
      '#default_value' => $configuration['alignment'] ?? 'items-start',
      '#description' => t('Left or center align section heading and preamble.'),
      '#options' => [
        'items-start' => $this->t('Left'),
        'items-center' => $this->t('Center'),
      ],
    ];

    $form['background'] = [
      '#type' => 'select',
      '#title' => $this ->t('Section background color'),
      '#default_value' => $configuration['background'] ?? 'transparent',
      '#description' => t('Applies a background color on the section.'),
      '#options' => [
        'transparent' => $this->t('Transparent'),
        'gray-100' => $this->t('Light gray'),
      ],
    ];

    $form['margin'] = [
      '#type' => 'select',
      '#title' => $this->t('Adjust margin'),
      '#default_value' => $configuration['margin'] ?? 'none',
      '#description' => t('Adds margin to the section to control the spacing to section before/after.'),
      '#options' => [
        'none' => $this->t('None'),
        'top' => $this->t('Add margin top'),
        'bottom' => $this->t('Add margin bottom'),
        'top_bottom' => $this->t('Add margin top and bottom'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['section_heading'] = $form_state->getValue('section_heading');
    $this->configuration['heading_level'] = $form_state->getValue('heading_level');
    $this->configuration['section_preamble'] = $form_state->getValue('section_preamble');
    $this->configuration['alignment'] = $form_state->getValue('alignment');
    $this->configuration['background'] = $form_state->getValue('background');
    $this->configuration['margin'] = $form_state->getValue('margin');
  }
}
