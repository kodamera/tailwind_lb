<?php

namespace Drupal\tailwind_lb\Routing;

use Drupal\Core\Routing\RouteBuildEvent;
use Drupal\Core\Routing\RoutingEvents;
use Drupal\tailwind_lb\Form\DiscardLayoutChangesForm;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Override routes for the Layout Builder UI.
 */
class LayoutBuilderUiRoutes implements EventSubscriberInterface {

  /**
   * Alters existing routes for a specific collection.
   *
   * @param \Drupal\Core\Routing\RouteBuildEvent $event
   *   The route build event
   */
  public function onAlterRoutes(RouteBuildEvent $event) {
    $collection = $event->getRouteCollection();

    /** @var \Symfony\Component\Routing\Route $route */
    if ($route = $collection->get('layout_builder.choose_block')) {
      $route->setDefault('_controller', '\Drupal\tailwind_lb\Controller\ChooseBlockController::inlineBlockList');
      $route->setDefault('_title', 'Choose component');
    }
    //
    /** @var \Symfony\Component\Routing\Route $route */
    if ($route = $collection->get('layout_builder.add_block')) {
      $route->setDefault('_title', 'Configure component');
    }

    /** @var \Symfony\Component\Routing\Route $route */
    if ($route = $collection->get('layout_builder.update_block')) {
      $route->setDefault('_title', 'Configure component');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // Run after \Drupal\layout_builder\Routing\LayoutBuilderRoutes.
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -200];
    return $events;
  }

}
