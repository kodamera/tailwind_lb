<?php

namespace Drupal\tailwind_lb\Commands;

use Drupal\breakpoint\BreakpointInterface;
use Drupal\breakpoint\BreakpointManagerInterface;
use Drupal\image\Entity\ImageStyle;
use Drupal\responsive_image\Entity\ResponsiveImageStyle;
use Drush\Commands\DrushCommands;

class ImageStyleCommands extends DrushCommands {

  /**
   * The breakpoint manager service.
   *
   * @var \Drupal\breakpoint\BreakpointManagerInterface
   */
  protected $breakpointManager;

  /**
   * A map of breakpoints.
   *
   * @var array
   */
  protected $breakpointMap = [
    'default' => 'tiny',
    'tiny' => 'sm',
    'sm' => 'md',
    'md' => 'lg',
    'lg' => 'xl',
    'xl' => '2xl',
    '2xl' => '3xl',
    '3xl' => '4xl',
    '4xl' => '4xl',
  ];

  /**
   * ImageStyleCommands constructor.
   *
   * @param \Drupal\breakpoint\BreakpointManagerInterface $breakpoint_manager
   */
  public function __construct(BreakpointManagerInterface $breakpoint_manager) {
    parent::__construct();
    $this->breakpointManager = $breakpoint_manager;
  }

  /**
   * Generates image styles from breakpoints.
   *
   * @command tailwind_lb:image_styles:generate
   */
  public function generate(): void {
    $breakpoint_groups = $this->breakpointManager->getGroups();

    $image_styles = [];

    foreach ($breakpoint_groups as $breakpoint_group_id => $breakpoint_group) {
      $breakpoints = array_reverse($this->breakpointManager->getBreakpointsByGroup($breakpoint_group_id));
      foreach ($breakpoints as $breakpoint_id => $breakpoint) {
        preg_match('/^tailwind_lb\.(\d+)/', $breakpoint->getGroup(), $region_matches);
        if (empty($region_matches)) {
          continue;
        }

        $region = $region_matches[1];
        $style = 'fixed';
        if (strpos($breakpoint->getGroup(), 'full') !== FALSE) {
          $style = 'full';
        }

        $sizes = explode('.', $breakpoint_id);
        $size = end($sizes);

        foreach ($breakpoint->getMultipliers() as $multiplier) {

          $image_style_name = "{$style}_{$region}_{$size}_{$multiplier}";

          $next_breakpoint_id = $this->getNextBreakpointId($size);
          $next_breakpoint = $breakpoints["{$breakpoint_group_id}.{$next_breakpoint_id}"] ?? $breakpoint;

          $width = $this->getBreakpointWidth($next_breakpoint);

          $region_modifier = ((int) $region) / 100;
          $padding = $style === 'fixed' ? 48 : 0;

          if ($width <= 768) {
            $region_modifier = 1;
          }

          if ($style === 'fixed' && $width >= 1280) {
            $width = 1280;
          }

          $multiplier_value = (int) $multiplier;

          $value = ceil(($region_modifier * $width * $multiplier_value)) - $padding;

          $image_styles[$breakpoint->getGroup()][$image_style_name] = [
            'region' => $region,
            'breakpoint_id' => $breakpoint_id,
            'width' => $value,
            'multiplier' => $multiplier,
            'style' => $style,
          ];
        }
      }
    }

    if (empty($image_styles)) {
      $this->logger()
        ->warning(dt('No image styles generated. Check your breakpoints file.'));
      return;
    }

    $this->createImageStyles($image_styles);

    $this->createResponsiveImageStyles($image_styles);

    $this->logger()->success(dt('Image styles generated.'));
  }

  /**
   * Get the next breakpoint as defined in the breakpoint map.
   *
   * @param $breakpoint_id
   *   A breakpoint identifier.
   *
   * @return string
   *   The next breakpoint.
   */
  public function getNextBreakpointId($breakpoint_id): string {
    return $this->breakpointMap[$breakpoint_id];
  }

  /**
   * @param array $breakpoint
   *
   * @return int
   *   The width.
   */
  public function getBreakpointWidth(BreakpointInterface $breakpoint): int {
    preg_match('/(\d+)px/', $breakpoint->getMediaQuery(), $width_matches);
    return (int) $width_matches[1];
  }

  /**
   * Create image styles.
   *
   * @param array $image_styles
   */
  public function createImageStyles(array $image_styles) {
    foreach ($image_styles as $group_id => $group_styles) {
      foreach ($group_styles as $image_style_name => $properties) {
        /** @var \Drupal\image\ImageStyleInterface $image_style */
        $image_style = ImageStyle::load($image_style_name);
        if (!$image_style) {
          $image_style = ImageStyle::create([
            'name' => $image_style_name,
            'label' => $image_style_name,
          ]);
        }
        $effects = $image_style->getEffects();
        foreach ($effects as $effect) {
          $image_style->deleteImageEffect($effect);
        }
        $image_style->addImageEffect([
          'id' => 'image_scale',
          'data' => [
            'width' => $properties['width'],
            'upscale' => TRUE,
          ],
        ]);
        $image_style->save();
      }
    }
  }

  /**
   * Creates responsive image styles.
   *
   * @param array $image_styles
   */
  public function createResponsiveImageStyles(array $image_styles): void {
    foreach ($image_styles as $group_id => $group_styles) {
      /** @var \Drupal\responsive_image\ResponsiveImageStyleInterface $responsive_image_style */
      $responsive_image_style = ResponsiveImageStyle::load($group_id);

      if (!$responsive_image_style) {
        $copy = array_values($group_styles);
        $first = array_shift($copy);
        $style = $first['style'];
        $region = $first['region'];
        $responsive_image_style = ResponsiveImageStyle::create([
          'id' => $group_id,
          'label' => $group_id,
          'breakpoint_group' => $group_id,
          'fallback_image_style' => "{$style}_{$region}_default_1x",
        ]);
      }

      foreach ($group_styles as $image_style_name => $properties) {
        $responsive_image_style->addImageStyleMapping($properties['breakpoint_id'], $properties['multiplier'], [
          'image_mapping_type' => 'image_style',
          'image_mapping' => $image_style_name,
        ]);
      }
      $responsive_image_style->save();
    }
  }

}
