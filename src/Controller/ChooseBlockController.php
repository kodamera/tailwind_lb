<?php

namespace Drupal\tailwind_lb\Controller;

use Drupal\Core\Url;
use Drupal\layout_builder_restrictions\Controller\ChooseBlockController as ModuleChooseBlockController;
use Drupal\layout_builder\SectionStorageInterface;

/**
 * Overrides core Layout Builder ChooseBlockController.
 *
 * This will merge together both inline blocks and regular blocks in one single
 * list.
 */
class ChooseBlockController extends ModuleChooseBlockController {

  /**
   * {@inheritdoc}
   */
  public function inlineBlockList(SectionStorageInterface $section_storage, $delta, $region): array {
    $build = parent::inlineBlockList($section_storage, $delta, $region);
    unset($build['back_button']);

    $definitions = $this->blockManager->getFilteredDefinitions('layout_builder', $this->getPopulatedContexts($section_storage), [
      'section_storage' => $section_storage,
      'delta' => $delta,
      'region' => $region,
    ]);
    foreach ($definitions as $block_id => $block) {
      if ($block_id === 'broken') {
        // Don't show broken blocks.
        continue;
      }

      $attributes = $this->getAjaxAttributes();
      $attributes['class'][] = 'js-layout-builder-block-link';
      $attributes['class'][] = 'inline-block-list__item';
      $link = [
        'title' => $block['admin_label'],
        'url' => Url::fromRoute('layout_builder.add_block',
          [
            'section_storage_type' => $section_storage->getStorageType(),
            'section_storage' => $section_storage->getStorageId(),
            'delta' => $delta,
            'region' => $region,
            'plugin_id' => $block_id,
          ]
        ),
        'attributes' => $attributes,
      ];

      $build['links']['#links'][] = $link;
    }

    return $build;
  }

}
