<?php

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\layout_builder\Form\DefaultsEntityForm;
use Drupal\layout_builder\Form\OverridesEntityForm;

/**
 * Implements hook_ENTITY_TYPE_view_alter().
 *
 * This intends to replace an inline image block with a responsive image that is
 * dynamic based on what layout and region it is in.
 */
function tailwind_lb_node_view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display) {
  if (!isset($build['_layout_builder'])) {
    return;
  }

  foreach ($build['_layout_builder'] as $lb_key => &$sections) {
    if (strpos($lb_key, '#') !== FALSE) {
      continue;
    }

    foreach ($sections as $section_key => &$section) {
      if (strpos($section_key, '#') !== FALSE) {
        continue;
      }

      foreach ($section as &$component) {
        if (!isset($component['#plugin_id']) || strpos($component['#plugin_id'], 'inline_block') === FALSE) {
          continue;
        }

        /** @var \Drupal\Core\Layout\LayoutDefinition $layout */
        $layout = $build['_layout_builder'][$lb_key]['#layout'];

        // Get layout id info.
        $layout_id = $layout->id();

        // Get section fullwidth info.
        $fullwidth_setting = $sections['#settings']['fullwidth'] ?? FALSE;

        // Fullwidth layouts.
        $fullwidth_layouts = [
          'tlb_full_col',
        ];

        // Let other modules alter this array via hooks.
        \Drupal::moduleHandler()->alter('tailwind_lb_fullwidth_layouts', $fullwidth_layouts);

        // If layout is always fullwidth, force fullwidth setting to true.
        if (in_array($layout_id, $fullwidth_layouts)) {
          $fullwidth_setting = TRUE;
        }

        // Add above info to the configuration of blocks in the section.
        if (isset($component['#configuration'])) {
          $component['#configuration']['is_fullwidth'] = $fullwidth_setting;
          $component['#configuration']['layout_id'] = $layout_id;
        }

        $enabled_components = [
          'inline_block:image',
          'inline_block:image_with_text',
          'inline_block:image_with_text_vertical',
        ];
        if (!isset($component['#plugin_id']) || !in_array($component['#plugin_id'], $enabled_components, TRUE)) {
          continue;
        }

        /** @var \Drupal\block_content\BlockContentInterface $block */
        $block = $component['content']['#block_content'];
        /** @var \Drupal\media\MediaInterface $media_entity */
        $media_entity = $block->get('field_image')->entity;

        if ($media_entity === NULL) {
          continue;
        }

        /** @var \Drupal\file\Entity\File $media_file */
        $media_file = $media_entity->get('field_media_image')->entity;

        if ($media_file === NULL) {
          continue;
        }

        $alt_text = $media_entity->get('field_media_image')->alt;

        $region = $section_key;

        $width_map = [
          'tlb_full_col' => '100',
          'tlb_one_col' => '100',
          'tlb_two_col' => '50',
          'tlb_three_col' => '33',
          'tlb_three_col_grid' => '33',
        ];

        $style = '';
        if ($fullwidth_setting) {
          $style = '_full';
        }

        $width = $width_map[$layout_id] ?? '33';

        // Image with text is a single component where the image only takes up
        // half of the container. It should also only be used in one col
        // layouts, so we hard code it to use 50% width.
        if ($component['#plugin_id'] === 'inline_block:image_with_text') {
          $width = '50';
        }

        $column_settings = $sections['#settings']['column_widths'] ?? FALSE;

        if ($column_settings) {
          $columns = explode('-', $column_settings);
          $column_map = [
            'first' => 0,
            'second' => 1,
            'third' => 2,
          ];

          $width = $columns[$column_map[$region]] ?? FALSE;
        }

        // Image settings.
        $image_settings = [
          '#theme' => 'responsive_image',
          '#responsive_image_style_id' => "tailwind_lb.{$width}{$style}",
          '#uri' => $media_file->getFileUri(),
          '#attributes' => [
            'alt' => $alt_text,
          ],
        ];

        // Context for hook.
        $context = [
          'section_key' => $section_key,
          'layout_id' => $layout_id,
          'plugin_id' => $component['#plugin_id'],
          'component' => $component,
          'width' => $width,
        ];

        // Let other modules alter these settings via hooks.
        \Drupal::moduleHandler()->alter('tailwind_lb_layout_image_settings', $image_settings, $context);

        // Set image settings for image.
        $component['content']['field_image'] = $image_settings;
      }
    }
  }
}

/**
 * Implements hook_preprocess_HOOK() for block.
 */
function tailwind_lb_preprocess_block(&$variables) {
  $variables['is_editing_layout'] = FALSE;

  $route_name = \Drupal::routeMatch()->getRouteName();

  $editing_routes = [
    'layout_builder.overrides.node.view',
    'layout_builder.add_block',
  ];

  if (in_array($route_name, $editing_routes, TRUE)) {
    $variables['is_editing_layout'] = TRUE;
  }
}

/**
 * Implements hook_plugin_filter_TYPE__CONSUMER_alter().
 */
function tailwind_lb_plugin_filter_layout__layout_builder_alter(array &$definitions, array $extra) {
  /** @var \Drupal\Core\Layout\LayoutDefinition $definition */
  foreach ($definitions as $plugin_id => $definition) {
    if ($definition->getProvider() !== 'tailwind_lb') {
      unset($definitions[$plugin_id]);
    }
  }
}

/**
 * Implements hook_form_alter().
 */
function tailwind_lb_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $form_object = $form_state->getFormObject();
  if ($form_object instanceof OverridesEntityForm || $form_object instanceof DefaultsEntityForm) {
    $form['#attached']['library'][] = 'tailwind_lb/layout_builder';
  }
}

/**
 * Implements hook_theme().
 */
function tailwind_lb_theme($existing, $type, $theme, $path) {
  return [
    'block__layout_builder' => [
      'render element' => 'elements',
    ],
    'block__inline_block__text' => [
      'render element' => 'elements',
    ],
    'block__inline_block__image' => [
      'render element' => 'elements',
    ],
    'block__inline_block__video' => [
      'render element' => 'elements',
    ],
    'block__inline_block__image_with_text' => [
      'render element' => 'elements',
    ],
    'block__inline_block__image_with_text_vertical' => [
      'render element' => 'elements',
    ],
    'block__inline_block__image_with_text_overlay' => [
      'render element' => 'elements',
    ],
    'block__inline_block__text_with_applied_styles' => [
      'render element' => 'elements',
    ],
    'file_video' => [
      'render element' => 'items',
    ],
    'node__page_tailwind_lb' => [
      'template' => 'node--page-tailwind-lb',
      'base hook' => 'node',
    ],
    'responsive_image' => [
      'template' => 'responsive-image',
      'base hook' => 'responsive_image',
    ],
    'image' => [
      'template' => 'image',
      'base hook' => 'image',
    ],
    'field__field_media_oembed_video' => [
      'template' => 'field--field-media-oembed-video',
      'base hook' => 'field',
    ],
    'field__field_media_video_file' => [
      'template' => 'field--field-media-video-file',
      'base hook' => 'field',
    ],
  ];
}
